<?php

$user = "root";
$password = "Nepse1992";
$host = "localhost";
$database = "myBase";

function connectToDb() {
    global $user, $password, $host, $database;
    $db = new mysqli($host, $user, $password, $database);
    if ($db->connect_error) {
        echo 'Did not establish connection to db!';
        die("Connection to data base failed!");
    }
    $db->set_charset("utf8");
    return $db;
}

function searchDB($db, $query) {
    $counter = 0;


    $db = connectToDb();
    $result = mysqli_query($db, $query) or die("could not find inserted row");

    if ($result) {
        $emparray = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($emparray, $row);
        }
        return $emparray;
    } else {
        return "Error: " . $sql . "<br>" . mysqli_error($db);
    }
}

if (htmlspecialchars($_SERVER["REQUEST_METHOD"]) == "GET") {

    //Check if captcha is valid and submit comment
    if (isset($_GET["validationKey"]) && (isset($_GET["comment"])) && (isset($_GET["name"])) && (isset($_GET["name"]))) {
        $key = htmlspecialchars(($_GET["validationKey"]));
        $object = htmlspecialchars(($_GET["object"]));
        $comment = htmlspecialchars(($_GET["comment"]));
        $name = htmlspecialchars(($_GET["name"]));

        $fields = array(
            'secret' => '6LeXGhITAAAAACdJcYo-KU07j5-h7At3FSrxsId_',
            'response' => $key
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $data = json_decode($response);
        $isValid = $data->success;

        date_default_timezone_set('Europe/Oslo');
        $timestamp = date('d/m/Y H:i', time());

        if ($isValid) {
            $db = connectToDb();
            $sql = "INSERT INTO `ratings` (`object`, `comment`, `timestamp`, `name`) VALUES ('$object','$comment','$timestamp','$name');";
            $result = mysqli_query($db, $sql) or die("could not insert row");
            if ($result) {
                echo "success";
            } else {
                echo "failure";
            }
        } else {
            echo "failure";
        }
    } else if (isset($_GET["ratedSchool"]) && (isset($_GET["rating"]))) {
        $school = htmlspecialchars($_GET["ratedSchool"]);
        $rating = htmlspecialchars($_GET["rating"]);
        $db = connectToDb();

        $currentRating = implode(mysqli_fetch_assoc(mysqli_query($db, "SELECT `school_rating` FROM `schools` WHERE `school_short` = '$school'")));
        $noRatings = implode(mysqli_fetch_assoc(mysqli_query($db, "SELECT `school_noRatings` FROM `schools` WHERE `school_short` = '$school'")));


        $newRating = (($currentRating * $noRatings) + $rating) / ($noRatings + 1);


        $query = "UPDATE `schools` SET `school_rating` = '$newRating' WHERE `school_short` = '$school'";


        $result = mysqli_query($db, $query) or die("could not find inserted row");

        $newNoRatings = $noRatings + 1;
        mysqli_query($db, "UPDATE `schools` SET `school_noRatings` = '$newNoRatings' WHERE `school_short` = '$school'") or die("could not find inserted row");
        if ($result) {


            echo "success" . "<br>";
            echo "nye antall ratings: " . $newNoRatings . "<br>";
        } else {
            echo "failure";
        }
    } else if (isset($_GET["ratingForCourse"])) {
        $course_code = htmlspecialchars($_GET["ratingForCourse"]);
        $db = connectToDb();

        $query = "SELECT `subject_rating` FROM `subjects` WHERE `subject_code` = '$course_code'";

        $result = mysqli_query($db, $query) or die("could not find inserted row");
        $rating = implode(mysqli_fetch_assoc($result));
        if ($result) {

            echo $rating;
        } else {
            echo "failure";
        }
    } else if (isset($_GET["ratedCourse"]) && (isset($_GET["rating"]))) {
        $course_code = htmlspecialchars($_GET["ratedCourse"]);
        $rating = htmlspecialchars($_GET["rating"]);
        $db = connectToDb();

        $currentRating = implode(mysqli_fetch_assoc(mysqli_query($db, "SELECT `subject_rating` FROM `subjects` WHERE `subject_code` = '$course_code'")));
        $noRatings = implode(mysqli_fetch_assoc(mysqli_query($db, "SELECT `subject_noRatings` FROM `subjects` WHERE `subject_code` = '$course_code'")));


        $newRating = (($currentRating * $noRatings) + $rating) / ($noRatings + 1);


        $query = "UPDATE `subjects` SET `subject_rating` = '$newRating' WHERE `subject_code` = '$course_code'";


        $result = mysqli_query($db, $query) or die("could not find inserted row");

        $newNoRatings = $noRatings + 1;
        mysqli_query($db, "UPDATE `subjects` SET `subject_noRatings` = '$newNoRatings' WHERE `subject_code` = '$course_code'") or die("could not find inserted row");
        if ($result) {


            echo "success" . "<br>";
            echo "nye antall ratings: " . $newNoRatings . "<br>";
        } else {
            echo "failure";
        }
    } else if (isset($_GET["ratingForSchool"])) {
        $school = htmlspecialchars($_GET["ratingForSchool"]);
        $db = connectToDb();

        $query = "SELECT `school_rating` FROM `schools` WHERE `school_short` = '$school'";

        $result = mysqli_query($db, $query) or die("could not find inserted row");
        $rating = implode(mysqli_fetch_assoc($result));
        if ($result) {

            echo $rating;
        } else {
            echo "failure";
        }
    } else if (isset($_GET["countCourses"])) {
        // $school = htmlspecialchars($_GET["school"]);
        $db = connectToDb();

        $courses = searchDB($db, "SELECT `school_name` FROM `schools` WHERE 1");
        foreach ($courses as $course) {
            $schoolName = $course["school_name"];
            echo $schoolName . " amount: ";
            $result = mysqli_query($db, "SELECT COUNT(*) FROM subjects WHERE `subject_school` LIKE '$schoolName'");
            $schoolCounter = mysqli_fetch_row($result);
            $count = $schoolCounter[0];
            $updateCounter = mysqli_query($db, "UPDATE schools SET school_courses_count = '$count' WHERE school_name LIKE '$schoolName'");
            echo var_dump($updateCounter);
        }
    } else if (isset($_GET["getAllSchools"])) {
        $db = connectToDb();

        $result = searchDB($db, "SELECT `school_name` FROM `schools` WHERE 1");
        echo json_encode($result);
    } else if (isset($_GET["getAllCourses"])) {

        $db = connectToDb();
        $sql = "SELECT * FROM `subjects`";

        $result = searchDB($db, $sql);
        echo json_encode($result);
    } else if (isset($_GET["getCourse"])) {
        $string = htmlspecialchars($_GET["getCourse"]);
        $db = connectToDb();
        $sql = "SELECT * FROM `subjects` WHERE `subject_code` = '$string'";

        $result = searchDB($db, $sql);

        echo json_encode($result);
    } else if (isset($_GET["getCourseComments"])) {
        $string = htmlspecialchars($_GET["getCourseComments"]);
        $db = connectToDb();
        $sql = "SELECT * FROM `ratings` WHERE `object` = '$string'";

        $result = searchDB($db, $sql);

        echo json_encode($result);
    } else if (isset($_GET["searchCourses"])) {
        $string = htmlspecialchars($_GET["searchCourses"]);
        $db = connectToDb();
        $sql = "SELECT * FROM `subjects` WHERE `subject_name` LIKE '%$string%' OR `subject_code` LIKE '%$string%'";

        $result = searchDB($db, $sql);

        echo json_encode($result);
    } else if (isset($_GET["searchSchools"])) {
        $string = htmlspecialchars($_GET["searchSchools"]);
        $db = connectToDb();
        $sql = "SELECT * FROM `schools` WHERE `school_name` LIKE '%$string%' OR `school_short` LIKE '%$string%'";

        $result = searchDB($db, $sql);
        echo json_encode($result);
    }
}

if (htmlspecialchars($_SERVER["REQUEST_METHOD"]) == "POST") {

    if (isset($_GET["getAllSchools"])) {
        $school = htmlspecialchars($_GET["school"]);
        $db = connectToDb();

        $result = searchDB("SELECT username FROM users WHERE username = '$username' ");
        echo $result;
    }



    if (isset($_POST['username']) && isset($_POST['todo'])) {
        $username = htmlspecialchars($_POST["username"]);
        $todo = htmlspecialchars($_POST["todo"]);

        $db = connectToDb();
        $sql = "INSERT INTO users (username, pw, email) VALUES ('$username','Tom B. Erichsen','Skagen 21');";

        if (mysqli_query($db, $sql)) {
            $verifyInsert = "SELECT username FROM users WHERE username = '$username' ";


            $result = mysqli_query($db, $verifyInsert) or die("could not find inserted row");
            $emparray = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $emparray[] = $row;
            }
            echo json_encode($emparray);
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    }
}

    