var app = angular.module('app', ['ngRoute', 'ui.materialize', 'ngCookies', 'vcRecaptcha', 'angular-loading-bar', 'smoothScroll']);

//This service provides methods for cheking if a user has rated a spesific course or school.
app.factory('votingHandler', function ($cookies) {
    return {
        tagAsRated: function (elementID) {
            var now = new Date();
            var exp = new Date(now.getFullYear() + 1, now.getMonth(), now.getDate());
            $cookies.put(elementID, 'rated', {
                expires: exp
            });
        },
        checkIfRated: function (elementID) {
            if ($cookies.get(elementID) !== 'rated') {
                return true;
            }
            return false;
        }
    };
});


app.factory('starsForRating', function ($http, votingHandler) {

    /**
     * Constructor, with class name
     * ElementIdentifier is course code or school name short.
     */
    function starsForRating(elementIdentifier, totalStarsInstance) {
        // Public properties, assigned to the instance ('this')
        this.elementIdentifier = elementIdentifier;
        this.totalStarsInstance = totalStarsInstance;
        this.stars = ["star_border", "star_border", "star_border", "star_border", "star_border", "star_border"];
        this.currentRating = 0;

    }

    /**
     * Public method, assigned to prototype
     * Called by the view to display array of "star" or "star_border"
     */
    starsForRating.prototype.getStars = function () {
        return this.stars;
    };

    //Is avtivated on mouse-enter. Removes all stars, and fills stars up untill the current hovered star.
    starsForRating.prototype.displayCurrentRating = function ($index) {
        for (i = 0; i < this.stars.length; i++) {
            this.stars[i] = "star_border";
        }
        for (i = 0; i < $index + 1; i++) {
            this.stars[i] = "star";
        }
    };

    //Is activated on mouse-leave. Shows the last persisted rating.
    starsForRating.prototype.showPersistedRating = function () {
        var i = 0;
        while (i < this.currentRating) {
            this.stars[i] = "star";
            i++;
        }
        while (i < this.stars.length) {
            this.stars[i] = "star_border";
            i++;
        }
    };

    starsForRating.prototype.persistRating = function ($index) {
        this.currentRating = $index + 1;
        this.showPersistedRating();
    }

    starsForRating.prototype.registerRating = function ($index) {

        var that = this;
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'ratedCourse': this.elementIdentifier,
                'rating': $index + 1
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            that.totalStarsInstance.removeTotalRating();
            that.totalStarsInstance.findCourseRating();
            votingHandler.tagAsRated(that.elementIdentifier);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    }
    // else {
    //     Materialize.toast('Du har allerede gitt dine stjerner!', 4000);
    // }
//};

    return starsForRating;
});

app.factory('totalStars', function ($http) {

    /**
     * Constructor, with class name
     * ElementIdentifier is course code or school name short.
     */
    function totalStars(elementIdentifier) {
        // Public properties, assigned to the instance ('this')
        this.elementIdentifier = elementIdentifier;
        this.stars = ["star_border", "star_border", "star_border", "star_border", "star_border", "star_border"];
        this.findCourseRating();
    }

    totalStars.prototype.getStars = function () {
        return this.stars;
    }

    //returns a 2 digit total avrage.
    totalStars.prototype.getAvrageShortned = function () {
        return this.avrageShortned;
    }

    totalStars.prototype.setAvrage = function (value) {
        this.avrageShortned = value;
    }


    totalStars.prototype.findCourseRating = function () {
        var that = this;
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'ratingForCourse': that.elementIdentifier
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            var course_rating = response.data;
            var course_rating_stars = (Math.round(course_rating * 2)) / 2;

            that.setAvrage((Math.round((course_rating * 1 + 0.00001) * 100)) / 100);
            //This is ouputted to the view
            for (var i = 0; i <= course_rating_stars - 1; i++) {
                that.stars[i] = "star";

            }

            var tmp = course_rating % 1;
            if ((tmp > 0.25) && (tmp < 0.75)) {
                that.stars[course_rating_stars - 0.5] = "star_half";
            }

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

        totalStars.prototype.removeTotalRating = function () {
            var i = this.stars.length;
            while (i--) {
                this.stars[i] = "star_border";
            }
        };
    };


    return totalStars;
});

//Service that is used for checking if a set of elements has been "setToValid"
app.factory('countValidator', function () {

    //Constructor that takes a given number of steps that need to be validated that has been provided.
    function countValidator(steps) {
        // Public properties, assigned to the instance ('this')
        this.isValid = false;
        this.validationSteps = new Array(steps);

        for (var i = 0; i < steps; i++) {
            this.validationSteps[i] = 0;
        }
    }

    //Sets a perticular step to "valid" state
    countValidator.prototype.setToValid = function (index) {
        this.validationSteps[index] = 1;
        this.validate();
    }

    //Checks if all elements are valid, and updates the "isValid" variable variable accordingly.
    countValidator.prototype.validate = function () {

        var validation = true;
        this.validationSteps.forEach(checkIfValid)

        function checkIfValid(value, index, array) {
            if (array[index] === 0) {
                validation = false;
            }
            else {
            }
        }
        this.isValid = validation;
    }

    return countValidator;
});


app.controller('reCaptchaCTRL', function ($scope, $http, smoothScroll, vcRecaptchaService) {
    $scope.valid = "disabled";
    $scope.response = null;
    $scope.widgetId = null;
    $scope.model = {
        key: '6LeXGhITAAAAALVmD37kef5_XCcklzNIeRfHVYcd'
    };
    $scope.setResponse = function (response) {
        $scope.response = response;
        $scope.valid = "";
    };
    $scope.setWidgetId = function (widgetId) {
        $scope.widgetId = widgetId;
    };
    $scope.cbExpiration = function () {
        $scope.response = null;
    };
    $scope.submit = function () {

        if (($scope.comment.length > 0) && ($scope.name.length > 0)) {
            $http({
                method: 'GET',
                url: 'server/userHandling.php/',
                params: {
                    'validationKey': $scope.response,
                    'object': $scope.object,
                    'comment': $scope.comment,
                    'name': $scope.name
                }
            }).then(function successCallback(response) {
                var response = angular.fromJson(response);
                $scope.response = response.data;
                if ($scope.response === "success") {
                    var element = document.getElementById('comments')  ;
                    smoothScroll(element);
                    Materialize.toast('Takk for din vurdering!', 4000);
                    $scope.findComments();
                    $scope.valid = "disabled";
                }
                else {
                    Materialize.toast('Du har allerede postet din kommentar!', 4000);
                }

                //   $scope.courses = response.data;
            }, function errorCallback(response) {

                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        }
        else {
            Materialize.toast('Du må fylle ut alle feltene!', 4000);
        }
    };
});


app.config(function ($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'main.html',
        controller: 'appCtrl'
    }).
    when('/course/:courseCode', {
        templateUrl: 'Course-Page.html',
        controller: 'CoursePageCtrl'
    }).
    when('/school/:schoolName', {
        templateUrl: 'School-Page.html',
        controller: 'SchoolPageCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });
});
app.controller('appCtrl', function ($scope, $http, $location, $cookies) {

    $scope.collapsibleIcon = "expand_more";
    $scope.openCourse = function (elementID) {
        $location.path('/course/' + elementID);
    };
    $scope.openSchool = function (elementID) {
        $location.path('/school/' + elementID);
    };
    $scope.changeCIcon = function () {
        if ($scope.collapsibleIcon === "expand_more") {
            $scope.collapsibleIcon = "expand_less";
        }
        else {
            $scope.collapsibleIcon = "expand_more";
        }
    };
    $scope.checkForCourses = function () {
        if ($scope.courses) {
            return false;
        }
        else {
            return true;
        }
    };
    $scope.checkForSchools = function () {
        if ($scope.schools) {
            return false;
        }
        else {
            return true;
        }
    };
    $scope.search = function (keyEvent) {
        if (keyEvent.which === 13) {
            $scope.searchAll();
        }
    };
    $scope.searchAll = function () {
        $scope.searchCourses();
        $scope.searchSchools();
    };
    $scope.searchSchools = function () {


        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'searchSchools': $scope.string
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.schools = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.searchCourses = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'searchCourses': $scope.string
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.courses = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
        $('select').material_select();
    };
    $scope.getCourses = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/?getAllCourses'
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.data = response.data[0]
            $scope.courses = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.getSchools = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/?getAllSchools'
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.schools = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    $('select').material_select();
});
app.controller('CoursePageCtrl', function ($scope, $http, $routeParams, votingHandler, starsForRating, totalStars, countValidator, smoothScroll) {


    $scope.courseCode = $routeParams.courseCode;

    //Instanices service for tracking average rating.
    $scope.totalStars = new totalStars($scope.courseCode);
    $scope.avrageShortned = $scope.totalStars.getAvrageShortned();


    //Creating instances of "starsForRating" which is used for measuring aspects of course
    $scope.workLoad = new starsForRating($scope.courseCode, $scope.totalStars);
    $scope.teachingQuality = new starsForRating($scope.courseCode, $scope.totalStars);
    $scope.skillLevel = new starsForRating($scope.courseCode, $scope.totalStars);
    $scope.grading = new starsForRating($scope.courseCode, $scope.totalStars);

    //Creating a instancse of "starsValidator" service which is ised for checking that all "star meassurement fields" are filled.
    $scope.starsValidator = new countValidator(4);


    $scope.submitStars = function () {
        if (votingHandler.checkIfRated($scope.courseCode)) {
            $http({
                method: 'GET',
                url: 'server/userHandling.php/',
                params: {
                    'ratedCourse': $scope.courseCode,
                    'rating': $scope.teachingQuality.currentRating
                }
            }).then(function successCallback(response) {
                var response = angular.fromJson(response);
                $scope.totalStars.removeTotalRating();
                $scope.totalStars.findCourseRating();

                votingHandler.tagAsRated($scope.courseCode);
                Materialize.toast('Din rating er motatt, takk!', 4000);

                var element = document.getElementById('top')
                smoothScroll(element);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

        }

        else {
            Materialize.toast('Du har allerede gitt dine stjerner!', 4000);
        }
    }

    $scope.object = $scope.courseCode;
    $scope.findCourse = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'getCourse': $scope.courseCode
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.courses = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    $scope.findComments = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'getCourseComments': $scope.courseCode
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.courseComments = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.courses = null;
    $scope.findCourse();
    //   $scope.findCourseRating();
    $scope.findComments();
});


app.controller('SchoolPageCtrl', function ($scope, $http, $routeParams, votingHandler) {

    $scope.stars = ["star_border", "star_border", "star_border", "star_border", "star_border", "star_border"];
    $scope.starsRator = ["star_border", "star_border", "star_border", "star_border", "star_border", "star_border"];
    $scope.schoolName = $routeParams.schoolName;
    $scope.object = $scope.schoolName;
    $scope.findSchoolRating = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'ratingForSchool': $scope.schoolName
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.school_rating = response.data;
            $scope.school_rating_stars = (Math.round($scope.school_rating * 2)) / 2;
            $scope.avrageShortned = (Math.round(($scope.school_rating * 1 + 0.00001) * 100)) / 100;

            for (var i = 0; i <= $scope.school_rating_stars - 1; i++) {
                $scope.stars[i] = "star";
            }

            var $tmp = $scope.school_rating % 1;
            if (($tmp > 0.25) && ($tmp < 0.75)) {
                $scope.stars[$scope.school_rating_stars - 0.5] = "star_half";
            }
        }, function errorCallback(response) {
            echo("errorCallback from server");
        });
    };
    $scope.registerRating = function ($index) {
        if (votingHandler.checkIfRated) {
            $http({
                method: 'GET',
                url: 'server/userHandling.php/',
                params: {
                    'ratedSchool': $scope.schoolName,
                    'rating': $index + 1
                }
            }).then(function successCallback(response) {
                var response = angular.fromJson(response);
                $scope.removeTotalRating();
                $scope.findSchoolRating();
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
            votingHandler.tagAsRated($scope.schoolName);
        }
        else {
            Materialize.toast('Du har allerede gitt dine stjerner!', 4000);
        }
    };
    $scope.displayCurrentRating = function ($index) {
        for (i = 0; i < $index + 1; i++) {
            $scope.starsRator[i] = "star";
        }
    };
    $scope.removeTotalRating = function () {
        var i = $scope.stars.length;
        while (i--) {
            $scope.stars[i] = "star_border";
        }
    };
    $scope.removeCurrentRating = function () {
        var i = $scope.starsRator.length;
        while (i--) {
            $scope.starsRator[i] = "star_border";
        }
    };
    $scope.findSchool = function () {
        $http({
            method: 'GET',
            url: 'server/userHandling.php/',
            params: {
                'searchSchools': $scope.schoolName
            }
        }).then(function successCallback(response) {
            var response = angular.fromJson(response);
            $scope.schools = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.findSchool();
    $scope.findSchoolRating();
});

